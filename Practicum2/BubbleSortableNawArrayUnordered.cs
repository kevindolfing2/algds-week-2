using Alg1.Practica.Practicum1;
using Alg1.Practica.Utils;

namespace Alg1.Practica.Practicum2
{
    public class BubbleSortableNawArrayUnordered : NawArrayUnordered, IBubbleSort
    {
        public BubbleSortableNawArrayUnordered(int initialSize) : base(initialSize)
        {
        }

        public void BubbleSort()
        {
            int timesSorted = 0;
            
            while(timesSorted < _used - 1)
            {
                for(int i = 0; i < _used - timesSorted - 1; i++)
                {      
                    if(_nawArray[i].CompareTo(_nawArray[i + 1]) == 1)
                    {
                        _nawArray.Swap(i, i + 1);
                    }
                }
                timesSorted++;
            }
            /*
             * De hoge o waarde komt omdat bij bubbel sort als de array 2x zo groot wordt, dan moet hij 4x zoveel stappen zetten
             * De orde van ArrayToOrdered is O(log(n))
             * Bij bubble sort hoef je het object niet om te zetten
             */
        }

        public void BubbleSortInverted()
        {
            int timesSorted = 0;

            while (timesSorted < _used - 1)
            {
                for (int i = _used - 1; i >= 0 + timesSorted + 1; i--)
                {
                    if (_nawArray[i].CompareTo(_nawArray[i - 1]) == -1)
                    {
                        _nawArray.Swap(i, i - 1);
                    }
                }
                timesSorted++;
            }
        }
    }
}
