using Alg1.Practica.Utils;
using Alg1.Practica.Utils.Exceptions;
using Alg1.Practica.Utils.Models;
using Alg1.Practica.Practicum1;


namespace Alg1.Practica.Practicum2
{
    public class BinarySearchableNawArrayOrdered : NawArrayOrdered, IBinarySearch
    {
        public BinarySearchableNawArrayOrdered(int initialSize) : base(initialSize)
        {


        }

        public int FindBinary(NAW item)
        {
            int lower = 0;
            int upper = _used;
            
            int checkLoc;
            while (true)
            {
                checkLoc = (lower + upper) / 2;
               if(lower > upper)
                {
                    return -1;
                } 
                else
                {

                    if(_nawArray[checkLoc].CompareTo(item) == 0)
                    {
                        return checkLoc;
                    }
                    else if(_nawArray[checkLoc].CompareTo(item) == 1)
                    {
                        upper = checkLoc - 1;
                    }
                    else
                    {
                        lower = checkLoc + 1;
                    }
                 
                }
            }
            /* de complexiteit van linear zoeken is n(O)
             * Van binair is dit log(O). BInair is veel sneller omdat dit maar langzaam toeneemt
             */
        }

        public void AddBinary(NAW item)
        {
            int location = FindBinary(item);
             for (int i = _used - 1; i >= location; i--)
            {
                Array[i + 1] = Array[i];
            }
            Array[location] = item;
            _used++;
        }
    }
}
